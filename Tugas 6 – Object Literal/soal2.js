
listPurchased= [
    {"barang":"Sepatu Stacattu",
    "harga" : 1500000
    },
    {"barang":"Baju Zoro",
    "harga" : 500000
    },
    {"barang":"Baju H&N",
    "harga" : 250000
    },
    {"barang":"Sweater Uniklooh",
    "harga" : 175000
    },
    {"barang":"Casing Handphone",
    "harga" : 50000
    }
]
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(memberId == null && money == null){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
        // console.log("Mohon maaf, toko X hanya berlaku untuk member saja")
        // console.log(money)

    }
    
    else if(money <= 50000){
        return "Mohon maaf, uang tidak cukup"
    }
    else if(memberId == '' && money <= 50000){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if(memberId == ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"

    }
    else{
        cukup = true
        list_pur = []
        duit = money
        while (cukup) {
            for (i = 0; i < listPurchased.length; i++) {
                harga = listPurchased[i]['harga']
                barang = listPurchased[i]['barang']
                if (duit - harga >= 0) {
                    duit -= harga
                    // console.log(barang)
                    list_pur.push(barang)
                    // console.log(duit)
                }   
                else{
                    cukup = false
                }
            }
        }
        x = {
            "memberID": memberId,
            "money": money,
            "listPurchased": list_pur,
            "changeMoney": duit
        }
        return x
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja