function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var n = []
    for (p = 0; p < arrPenumpang.length; p++) { 
        awal = null
        for (i = 0; i < rute.length; i++) {
            if (arrPenumpang[p][1] == rute[i]) {
                awal = 0
            }
            if (arrPenumpang[p][2] == rute[i]) {
                break
            }
            if (awal >= 0) {
                awal += 1
            }
        }
        y = {
            "penumpang": arrPenumpang[p][0], 
            "naikDari":arrPenumpang[p][1], 
            "tujuan":arrPenumpang[p][2], 
            "bayar":awal * 2000
            }
        n.push(y)
    }
    return n    

  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]