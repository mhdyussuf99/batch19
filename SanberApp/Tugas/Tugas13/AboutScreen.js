import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Image,
    Text,
    TouchableOpacity,
    TextInput,
    ScrollView
} from 'react-native'


export default class App extends Component {

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.navBar}>

                </View>
                <View style={{
                    // flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Text style={{ fontStyle: "bold", fontSize: 50, paddingTop: 10, paddingBottom: 10, color: "#003366" }}>Tentang Saya</Text>
                    <Image source={require('./images/user.jpg')} style={{ width: 200, height: 200, borderRadius: 200 / 2 }} />
                    <Text style={{ fontStyle: "bold", fontSize: 20, paddingTop: 10, color: "#003366" }}>Muhammad Yussuf</Text>
                    <Text style={{ fontSize: 15, paddingTop: 2, paddingBottom: 10, color: "#3EC6FF" }}>React Native Developer</Text>


                </View>
                <ScrollView>
                <View style={{ paddingBottom: 10 }}>
                    <Text style={{ padding: 10 }}>Portofolio</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('./images/logo-sanber.png')} style={{ width: 50 }} />
                            <Text style={{ fontSize: 15, paddingTop: 2, paddingBottom: 10, color: "#3EC6FF" }}>@mhdyussuf</Text>
                        </View>
                        <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('./images/logo-sanber.png')} style={{ width: 50 }} />
                            <Text style={{ fontSize: 15, paddingTop: 2, paddingBottom: 10, color: "#3EC6FF" }}>@mhdyussuf</Text>
                        </View>

                    </View>
                </View>


                <View style={{ paddingBottom: 10 }}>
                    <Text style={{ padding: 10 }}>Hubungi Saya</Text>
                    <View style={{ flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('./images/logo-sanber.png')} style={{ width: 50 }} />
                            <Text style={{ fontSize: 15, paddingTop: 2, paddingLeft: 20, paddingBottom: 10, color: "#3EC6FF" }}>muhammad yussuf</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('./images/logo-sanber.png')} style={{ width: 50 }} />
                            <Text style={{ fontSize: 15, paddingTop: 2, paddingBottom: 10, paddingLeft: 20, color: "#3EC6FF" }}>@mhdyussuf</Text>
                        </View>
                        
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('./images/logo-sanber.png')} style={{ width: 50 }} />
                            <Text style={{ fontSize: 15, paddingTop: 2, paddingBottom: 10, paddingLeft: 20, color: "#3EC6FF" }}>@anaktkaksi</Text>
                        </View>

                    </View>
                </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 55,

    },
    navBar: {
        height: 55,
    },
    layout: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.05)',
    },
    box: {
        padding: 25,
        backgroundColor: 'steelblue',
        margin: 5,
    },
    Button1: {
        width: 90,
        height: 30,
        borderRadius: 90 / 2,
        backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Button2: {
        width: 90,
        height: 30,
        borderRadius: 90 / 2,
        backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container1: {
        width: 300,
        padding: 16
    },
})