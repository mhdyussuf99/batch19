import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, TextInput, Button } from 'react-native';

// const Card = ({ title }) => {
//     return (
//         <TouchableOpacity style={styles.Button2}>
//             <Text>{title}</Text>
//         </TouchableOpacity>
//     )
// }
const App = () => {
    return (



        <View style={styles.container}>
            <View style={{ paddingBottom: 10 }}>
                <Text>Username</Text>
                <TextInput placeholder="Username" style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }} />
            </View>
            <View style={{ paddingBottom: 10 }}>
                <Text>Password</Text>
                <TextInput placeholder="Password" style={{ height: 40, borderColor: 'gray', borderWidth: 1, padding: 10 }} />
            </View>
            <View style={{alignItems:"center", paddingBottom: 10 }}>
            <TouchableOpacity style={styles.Button1}>
             <Text style={{color:'white'}}>Login</Text>
            </TouchableOpacity>
            <Text style={{paddingTop:10, paddingBottom:10}}>atau</Text>
            <TouchableOpacity style={styles.Button2}>
             <Text style={{color:'white'}}>Daftar</Text>
            </TouchableOpacity>
            
            </View>
            

        </View>
    )
}

export default App
const styles = StyleSheet.create({
    Button1: {
        width: 90,
        height: 30,
        borderRadius: 90/2,
        backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Button2: {
        width: 90,
        height: 30,
        borderRadius: 90/2,
        backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        width: 300,
        padding: 16
    },
})