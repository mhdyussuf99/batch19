import React, { Component } from 'react'
import { 
    View, 
    StyleSheet,
    Image,
 } from 'react-native'

import Toggle from './AboutScreen'
import TextInANest from './components/logininput'

export default class App extends Component {

  state = {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }

  render() {
    const {flexDirection, alignItems, justifyContent} = this.state
    const layoutStyle = {flexDirection, justifyContent, alignItems}

    const primaryAxis = flexDirection === 'row' ? 'Horizontal' : 'Vertical'
    const secondaryAxis = flexDirection === 'row' ? 'Vertical' : 'Horizontal'

    return (
      <View style={styles.container}>
          <View style={styles.navBar}>
          
        </View>
        {/* <Toggle
          label={'Primary axis (flexDirection)'}
          value={flexDirection}
          options={['row', 'column']}
          onChange={(option) => this.setState({flexDirection: option})}
        />
        <Toggle
          label={primaryAxis + ' distribution (justifyContent)'}
          value={justifyContent}
          options={['flex-start', 'center', 'flex-end', 'space-around', 'space-between', 'space-evenly']}
          onChange={(option) => this.setState({justifyContent: option})}
        />
        <Toggle
          label={secondaryAxis + ' alignment (alignItems)'}
          value={alignItems}
          options={['flex-start', 'center', 'flex-end', 'stretch', 'baseline']}
          onChange={(option) => this.setState({alignItems: option})}
        /> */}
        <View style={{
            // flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'center'
        }}>
          
          <Image source={require('./images/logo-sanber.png')} style={{ width: 398}} />
          
        </View>
        <View style={{
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'center'
        }}>
          
          <TextInANest/>
          
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 55,

  },
  navBar: {
    height: 55,
    // backgroundColor: 'white',
    // elevation: 3,
    // paddingHorizontal: 15,
    // flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: 'space-between'
  },
  layout: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  box: {
    padding: 25,
    backgroundColor: 'steelblue',
    margin: 5,
  },
})