function range(startNum, finishNum) {
    var awal = startNum ;
    var akhir = finishNum  +1;

    if((startNum <= 1 && finishNum== null)|| startNum == null && finishNum == null){
        return -1
    }
    else{
        
        var x = [] ;
        if(awal>akhir){
            awal_nya = akhir -1
            akhir_nya = awal +1
            for (i = awal_nya; i < akhir_nya; i++) {
                x.push(i)

            }
            
            return x.reverse()
        }else{
            
            for (i = awal; i < akhir; i++) {
                x.push(i)

            }
        
        return x
        }

    }
}
console.log("---soal 1---")
console.log(range(1,10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())
console.log("-----------")

function rangeWithStep(startNum, finishNum, step) {
    var awal = startNum ;
    var akhir = finishNum  +1;

    if (awal>akhir){
        // awal = akhir -1
        akhir =akhir  - 2
        step_nya = step * -1
    }else{
        step_nya = step    
    }
    var x = [] ;

        for (var i = awal; step_nya > 0 ? i < akhir : i > akhir; i += step_nya) {
            x.push(i)
        }
    
    return x
}


console.log("----soal2---")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("-----------")

function sum(startNum, finishNum, step_nya) {
    var awal = startNum ;
    var akhir = finishNum  +1;

    var x =0
    if(startNum == 1 && finishNum == null && step_nya == null){
        return startNum
    }
    if(step_nya== null){
        step_nya = 1
    }else{
        step_nya = step_nya
    }
    if (awal>akhir){
        // awal = akhir -1
        akhir =akhir  - 2
        step_nya = step_nya * -1
    }else{
        step_nya = step_nya    
    }
    for (var i = awal; step_nya > 0 ? i < akhir : i > akhir; i += step_nya) {
        // console.log(awal+"="+i)
        x += i 
    }
    return x
    
}
console.log("---soal3---")
console.log(sum(1,10))
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("-----------")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
function dataHandling(){
    panjang_array = input.length;
    for (i = 0; i < panjang_array; i++) {
        console.log("Nomor ID: "+ input[i][0])
        console.log("Nama Lengkap: "+ input[i][1])
        console.log("TTL: "+input[i][2] + " "+ input[i][3])
        console.log("Hobi: "+ input[i][4])
        console.log(" ")

      }
}
console.log("---soal4---")
dataHandling()
console.log("-----------")

function balikKata(input){
    var text = []
    for (var i = input.length - 1; i >= 0; i--) {
        text.push(input[i])
    }
    text = (text.toString()).replace(/,/g, '')

    return text
}

console.log("---soal5---")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("-----------")


var input = ["0001", "Roman Alamsyah Elsharawy", "Bandar Lampung", "21/05/1989", "Membaca"] 
function dataHandling2(data){
    data.push("Pria", "SMA Internasional Metro")

    data_baru = []
    data_baru.push(data[0],data[1],"Provinsi "+data[2],data[3],data[5],data[6])
    tanggal = data[3].split("/")
    bulan = parseInt(tanggal[1])
    // bulan = data[4]
    switch (bulan) {
        case 1:
            bulan = "Januari";
        break;
        case 2:
            bulan = "Februari";
        break;
        case 3:
            bulan = "Maret";
        break;
        case 4:
            bulan = "April";
        break;
        case 5:
            bulan = "Mei";
        break;
        case 6:
            bulan = "Juni";
        case 7:
            bulan = "Juli";
        break;
        case 8:
            bulan = "Agustus";
        break;
        case 9:
            bulan = "September";
        break;
        case 10:
            bulan = "Oktober";
        break;
        case 11:
            bulan = "November";
        break;
        case 12:
            bulan = "Desember";
    }
    data_nama = (data[1].split('').slice(0,15).toString()).replace(/,/g, '')

    console.log(data_baru)
    console.log(bulan)
    console.log(tanggal)
    console.log(tanggal[0]+"-"+tanggal[1]+"-"+tanggal[1])
    console.log(data_nama)

    
}
console.log("---soal6---")
dataHandling2(input)
console.log("-----------")
